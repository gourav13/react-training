import { useEffect,useState,useRef } from 'react';
import React from "react";
import Slider from "react-slick";
function ProductSlider({ product, currentVariant }) {
  const [nav1, setNav1] = useState(null);
  const [nav2, setNav2] = useState(null);
  const slickGoto=(e)=>{
    if(e!=null){
      if(currentVariant.featured_image){
        let index = currentVariant.featured_image.position;
        e.slickGoTo(index);
      }
    }
  }
  let { media, images } = product;
  let currentVariantImages = [];
  if (!media) {
    return null;
  }
  if (currentVariantImages.length <= 0) {
    currentVariantImages = images;
  }
  var settings = {
    dots: false,
    infinite: true,
    loop: true,
    speed: 500,
    fade: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    nextArrow: <span><img src="https://cdn.shopify.com/s/files/1/0997/6284/files/Asset_1_1.svg?v=1628501758" style={{ width: '25px', margin: '7px 9px' }} alt='next-arrow' /></span>,
    prevArrow: <span><img src="https://cdn.shopify.com/s/files/1/0997/6284/files/Arrow_left.svg?v=1628501809" style={{ width: '25px', margin: '7px 5px' }} alt='prev-arrow' /></span>
  };
  var settings2 = {
    dots: false,
    infinite: true,
    loop: true,
    speed: 500,
    arrows: false,
    centerMode: true,
    slidesToShow: 5,
    slidesToScroll: 1
  };
  const sliderImages = currentVariantImages.map((item) => <div key={item} className="image"><img key={item} src={item} /> </div>)
  
  return (
    <>
      <div className="main-image-wrap">
        <Slider asNavFor={nav2} ref={(slider1) => {setNav1(slider1); slickGoto(slider1);}} {...settings}>
          {sliderImages.length > 0 ? sliderImages : ''}
        </Slider>
      </div>
      <div className="thumbnail-images">
        <Slider
          asNavFor={nav1}
          ref={(slider2) => setNav2(slider2)}
          swipeToSlide={true}
          focusOnSelect={true}
          {...settings2}
        >
          {sliderImages.length > 0 ? sliderImages : ''}
        </Slider>
      </div>
    </>
  );

}
export default ProductSlider;