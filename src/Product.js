import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import ProductSlider from './ProductSlider';
import VariantSelector from "./VariantSelector";
function Product() {
  const [product, setData] = useState({})
  const [currentVariant, setVariant] = useState({})
  const [productQty, setQty] = useState(1)
  let { id } = useParams();

  useEffect(() => {
    var url_string = window.location.href;
    var newurl = new URL(url_string);
    var variant = newurl.searchParams.get("variant");
    var availQuery = variant ? '?variant='+variant+'&':'?';
    const fetchProduct = async () => {
      let url = `https://lucent-theme-6.myshopify.com/products/${id}${availQuery}view=react`;
      let res = await fetch(url);
      res = await res.json();
      if (res) {
        console.log(res);
        let productJson = res.product_data;
        let selectedVariant = {};
        if (res.selected_variant != null) {
          selectedVariant = res.selected_variant;
        }
        else {
          selectedVariant = productJson.selected_or_first_available_variant ? productJson.selected_or_first_available_variant : productJson.variants[0];
        }
        setVariant(selectedVariant);
        setData(productJson);
      }
    }
    fetchProduct();
  }, [id]);
  const productQuantity = (type) => {
    let qty = productQty;
    if (type === 'plus') {
      qty = qty + 1;
    } else {
      if (qty > 1) {
        qty = qty - 1;
      }
    }
    setQty(qty);
  }
  const handleSubmit = (e) => {
    e.preventDefault();
    addtoCart();
  }
  const addtoCart = () => {
    if (!currentVariant.id) {
      console.log("Variant id is missing");
      return;
    }
    let formData = {
      'items': [{
        'id': currentVariant.id,
        'quantity': productQty
      }]
    };
    fetch('/cart/add.js', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        "accepts": "application/json"
      },
      body: JSON.stringify(formData)
    })
      .then(response => {
        if (response.ok) {
          console.log(response);
        }
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  }
  if (!product ||!currentVariant.id) {
    return null
  }
  return (
    <>
      <div className="product-content-main">
        <div className="product-image-main">
          <ProductSlider product={product} currentVariant={currentVariant}/>
        </div>
        <div className="product-text-main">
          <div className="text-section-inner">
            <h4 className="title-main">{product?.title}</h4>
            <div className="price-wrap">
              <span className="price-main">₹ {parseInt(currentVariant?.price / 100)}</span>
              {currentVariant?.compare_at_price > currentVariant?.price &&
                <span className="compare-price">₹ {parseInt(currentVariant?.compare_at_price / 100)}</span>
              }
            </div>
            <div className="product-form-wrap">
              <form id="AddToCartForm" data-vid={`${currentVariant.id}`} onSubmit={handleSubmit}>
                <VariantSelector product={product} currentVariant={currentVariant} onVarintChange={setVariant} />
                <div className="quantity-selector">
                  <label className="qty-label">Quantity</label>
                  <div className="quantity-wrapper">
                    <div className="qty-minus" onClick={(e) => productQuantity('minus')}>-</div>
                    <div className="input-value">{productQty}</div>
                    <div className="qty-plus" onClick={(e) => productQuantity('plus')}>+</div>
                  </div>
                </div>
                {currentVariant.available ? (
                  <button className="btn AddToCart" name="add" type="submit" >
                    <span className="AddToCartText">
                      Add to cart
                    </span>
                  </button>
                ) : (
                  <button className="btn AddToCart disabled" disabled name="add" type="button">
                    <span className="AddToCartText">
                      Sold Out
                    </span>
                  </button>
                )
                }
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
export default Product;