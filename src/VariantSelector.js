import React from 'react';
function VariantSelector({ product, currentVariant, onVarintChange }) {
  let arr = [];
  const uniqueOption = (option) => {
    if (arr.length === 0) {
      arr.push(option);
      return true;
    }
    else {
      if (arr.includes(option, 0)) {
        return false;
      }
      else {
        arr.push(option)
        return true;
      }
    }
  }
  const selectChange = (e) => {
    if (product) {
      console.log(currentVariant);
      let values = [];
      currentVariant.options.forEach((op, index) => {
        let obj = {};
        obj.index = "option" + (index + 1);
        obj.value = op;
        values.push(obj);
      })
      let opIndex = e.target.getAttribute("data-option");
      let index = e.target.getAttribute("data-index");
      let value = e.target.getAttribute("data-value");
      values[opIndex].index = index;
      values[opIndex].value = value;
      let variant = findVariant(values);
      if (variant) {
        queryStringVariant(variant);
        onVarintChange(variant);
      }
    }
  }
  const findVariant = (values) => {
    let variants = product.variants;
    let size = values.length;
    for (let i = 0; i < variants.length; i++) {
      let variant = variants[i];
      if (size === 1) {
        if (variant[values[0].index] === values[0].value) {
          return variant;
        }
      }
      else if (size === 2) {
        if (variant[values[0].index] === values[0].value && variant[values[1].index] === values[1].value) {
          return variant;
        }
      }
      else {
        if (variant[values[0].index] === values[0].value && variant[values[1].index] === values[1].value && variant[values[2].index] === values[2].value) {
          return variant;
        }
      }
    }
  }
  const queryStringVariant = (variant) => {
    let value = variant.id
    let key = "variant";
    let uri = window.location.href;
    let re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    let separator = uri.indexOf('?') !== -1 ? "&" : "?";
    let newUrl = "";
    if (uri.match(re)) {
      newUrl = uri.replace(re, '$1' + key + "=" + value + '$2');
    }
    else {
      newUrl = uri + separator + key + "=" + value;
    }
    window.history.replaceState({}, document.title, newUrl);
  }
  return (
    <>
      {product.options?.map((option, index) => {
        return (
          <div className="swatches">
            <div className="swatch" data-option-index={`${index}`}>
              <div><p className="swatch-title">{option}</p></div>
              {product.variants.map((variant) => {
                return (
                  <>
                    {uniqueOption(variant.options[index]) &&
                      <div data-option={`${index}`} data-value={`${variant.options[index]}`} data-index={`option${index + 1}`} className={`swatch-element ${currentVariant.available ? `` : `${currentVariant?.options[product.options.length - 1] == variant.options[index] ? 'strike-off' : ''}`} ${currentVariant?.options[index] === variant.options[index] ? 'active' : ''}`} onClick={selectChange}>
                        {variant.options[index]}
                      </div>
                    }
                  </>
                )
              })}
            </div>
          </div>
        )
      })}
    </>
  )
}
export default VariantSelector;