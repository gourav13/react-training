import React, { useState } from "react";
import { NavLink } from "react-router-dom";
const NavigationMenu = () => {
  const [products] = useState([
    {
      name: "Line Dress",
      id: "a-line-dress-white"
    },
    {
      name: "Air buds",
      id: "air-buds-mini"
    },
    {
      name: "Pyjamas",
      id: "classic-pyjamas-tan-stripes"
    },
    {
      name: "Kurta",
      id: "chandni-chowk-kurta-indigo"
    }
  ]);
  return (
    <>
      <div className="navigation-menu">
        <NavLink exact="true" activeClassName="active" to="/">
          Home
        </NavLink>
        {products.map(product => (
          <NavLink activeClassName="active" to={`/product/${product.id}`}>
            {product.name}
          </NavLink>
        ))}
      </div>
    </>
  );
}
export default NavigationMenu;