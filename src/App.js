import React from 'react';
import { Route, Routes } from 'react-router-dom';
import Home from './Home';
import Product from './Product';
import NavigationMenu from './NavigationMenu';
function App() {
  return (
    <>
      <NavigationMenu />
      <Routes>
        <Route exact="true" path="/" element={<Home />} />
        <Route exact="true" path="/product/:id" element={<Product />} />
      </Routes>
    </>
  )
}
export default App;